## Class Action News


![](https://i.imgur.com/MmlHKU6.png?1)
---
NextGen Mapping, Inc.

 
Hello Everyone,

This repository is to update you on the work and development that is going into the Class Action software that is being developed. This repository will also allow &quot;you&quot; (the public) to voice your issues so that we can better the software by responding and resolving as many of the issues as we can. If you switch between the &quot;branches&quot; of the repository (Development vs Master) you will be able to see our nightly build news as well as what we&#39;re working on consistently. Until then Please see all the news below.

11/5/2018

- Bugfixes to the new vehicle component
- Original file names in the system
- Fixes to the dashboard
- Fixes to the user managment section
- Removed unused menu items from master accounts
- Impersonation is now available also for plaintiffs user
- Added default login page option in profile
- Added impersonation funcionality
- Added "reset password" to the email check in signup, only for organic
- The feedback is now smaller on the internal pages
- Fixed the Claimants document filters
- Original file name now available
- Fixes to the vehicle component
- Fixed email action link
- Is now possible to bulk assign a manager
- Added notification store
- Fixed email blank space
- Fiexed cards title size on mobile view
- Update to vehicle component layout, now with granular details
- Update to the file upload, now storing also the original file name
- Fix to login media-query for mobile version
- Only the plaintiff is now able to edit informations
- Removed contact details from feedback, pulled automatically from the account / litigation if available
- Feedback button is now smaller
- Fix to the vehicle update
- Added the ability to add a comment on Claim item approve
- Added the ability to add a comment on Claim item reject
- Added the email or SMS notification (based on the contact info) when a change is made to the claim
- Fixed email title on reject and approve
- Added the default currency, the account owner is now able to change the currency

10/25/2018

- Email lo-dash implemented, the email check is now run while typing
- Fixed the email exist bug, the form can not be sent if the email already exist
- Removed the contract from the sign-up process
- Is not possible, for the claimant, to edit the personal information
- Added the Feedback functionality
- Fixed some issues with the email template
- Fixed the Address format in the account setup
- Better zoom map level on the SearchAddress functionality
- Comapny toggle no more visible if the claims is compiled for a third person
- Dynamic * on the email and phone fields
- Fixed the Recaptcha timing
- Fixes to the UI in mobile version
- New contact information in Account section. The contact information will be used in the email template.
- Spouse driver license scan
- New email template for every communication procuded by the software
___
##### Changes that are coming shortly.

###### Huge Security Update (Encryption Server Side of Data)
We take security very seriously here. On top of the SSL that we already have in place to secure the data transmission from your computer's browser to our server. We are taking extra steps to secure the data on our server as well using a proprietary and unique encryption method. Once implemented all the users
that have already signed up on our system will be notified how they were originally notified with what is known as an encryption key. Any new users will be provided with one at the time of sign-up. This key is a very important key. **DO NOT** delete this key. If you wish to update or change any information on your profile that consists of sensitive data (i.e. 
SSN, Drivers License etc...) you will need this key in order to do so. This key cannot be recovered for security purposes. We have the ability to regenerate you a new key but we do not have the ability 
to read your key and/or access it. This key is held by you and only you. Once your key has been reset you will be asked to 
re-enter the data you have provided as this data will be no longer valid under the new key. Understand that this is for your security.

Also note: Once a claim has been fully approved. We will not be able to reset your key.  
___
11/16/2018

 Today we launched our first full release with all the issues that were reported marked as resolved.